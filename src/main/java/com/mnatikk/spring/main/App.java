package com.mnatikk.spring.main;

import com.mnatikk.spring.main.logger.EventLogger;
import com.mnatikk.spring.main.beans.Client;
import com.mnatikk.spring.main.beans.Event;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by mnatikk on 22.01.18.
 */
public class App {
    Client client;
    EventLogger eventLogger;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        App app = (App) context.getBean("app");

        Event event = context.getBean(Event.class);
        app.logEvent(event, "Some event for user 1");
        event = context.getBean(Event.class);
        app.logEvent(event, "Some event for user 2");

        context.close();
    }

    public App(Client client, EventLogger eventLogger) {
        super();
        this.client = client;
        this.eventLogger = eventLogger;
    }

    public void logEvent(Event event, String msg){
        String message = msg.replaceAll(client.getId(), client.getFullName());
        event.setMsg(msg);
        eventLogger.logEvent(event);
    }


}
