package com.mnatikk.spring.main.logger;

import com.mnatikk.spring.main.beans.Event;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mnatikk on 24.01.18.
 */
public class CacheFileEventLogger extends FileEventLogger {

    private int cacheSize;
    private List<Event> cache;

    public CacheFileEventLogger(String fileName, int cacheSize) {
        super(fileName);
        this.cacheSize = cacheSize;
        this.cache = new ArrayList<Event>(cacheSize);
    }

    public void logEvent(Event event){
        cache.add(event);
        if (cache.size() == cacheSize) {
            writeEventsFromCache();
            cache.clear();
        }
    }

    private void writeEventsFromCache() {
        cache.stream().forEach(super::logEvent);
    }

    public void destroy() {
        System.out.println("destroy");
        if ( ! cache.isEmpty()) {
            System.out.println("cache");
            writeEventsFromCache();
        }
    }

    public void init() throws IOException {
        System.out.println("init");
    }
}
