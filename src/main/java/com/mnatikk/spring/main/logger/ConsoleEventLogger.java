package com.mnatikk.spring.main.logger;

import com.mnatikk.spring.main.beans.Event;

/**
 * Created by mnatikk on 22.01.18.
 */
public class ConsoleEventLogger implements EventLogger{
    public void logEvent (Event event){
        System.out.println(event.toString());
    }
}
