package com.mnatikk.spring.main.logger;

import com.mnatikk.spring.main.beans.Event;

import java.io.IOException;

/**
 * Created by mnatikk on 22.01.18.
 */
public interface EventLogger {
    public void logEvent (Event event);
}
