package com.mnatikk.spring.main.logger;

import com.mnatikk.spring.main.beans.Event;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by mnatikk on 24.01.18.
 */
public class FileEventLogger implements EventLogger {
    File file;
    String fileName;

    public FileEventLogger(String fileName) {
        this.fileName = fileName;
    }

    public void logEvent(Event event)  {

        try {
            FileUtils.writeStringToFile(file, event.toString(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void init() throws IOException {
        this.file = new File(fileName);
        if (!file.canWrite()) throw new IOException();
    }
}
